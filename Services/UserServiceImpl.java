@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService, UserDetailsService {

  private final RoleService roleService;

  private final UserRepository userRepository;
  private final ResetRepository resetRepository;

  private final BCryptPasswordEncoder bCryptPasswordEncoder;
  private final RoleMapper roleMapper;
  private final UserMapper userMapper;
  private final CommonHelper commonHelper;
  private final DateTimeHelper dateTimeHelper;

  public UserDTO signup(UserDTO userDto) {
    String email = userDto.getEmail();

    boolean isUserExist = userRepository.findByEmail(email).isPresent();
    if (isUserExist) {
      throw new ApiRequestException(String.format(EMAIL_EXISTS_MESSAGE, email));
    }

    UUID userId = UUID.randomUUID();
    String encodedPassword = bCryptPasswordEncoder.encode(userDto.getPassword());

    userDto.setId(userId);
    userDto.setEnabled(false);
    userDto.setLocked(false);
    userDto.setRole(ADMIN);
    userDto.setPassword(encodedPassword);
    userDto.setCreatedAt(CURRENT_LOCAL_DATETIME);

    User user = userMapper.fromDTOToEntity(userDto);


    List<Role> roles = userDto.getRoles().stream()
      .map(role -> roleMapper.fromDTOToEntity(roleService.findByName(role)))
      .collect(Collectors.toList());

    List<UserRole> userRoles = roles.stream()
//      .map(role -> new UserRole(role, CURRENT_LOCAL_DATETIME))
      .map(role ->
        new UserRole(
          new UserRoleId(userId, role.getId()), user, role, CURRENT_LOCAL_DATETIME
        )
      )
      .collect(Collectors.toList());
    user.setRoles(userRoles);

    Token confirmationToken = new Token(
      UUID.randomUUID().toString(),
      LocalDateTime.now(),
      LocalDateTime.now().plusDays(7L)
    );
    user.setToken(confirmationToken);

    confirmationToken.setUser(user);
    userRepository.save(user);

    return userMapper.fromEntityToDTO(user);
  }

}
