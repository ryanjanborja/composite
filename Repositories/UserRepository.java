@Repository
@Transactional(readOnly = true)
public interface UserRepository extends JpaRepository<User, UUID> {

  List<User> findAllByOrderByCreatedAtDesc();

  @Query("SELECT s FROM User s WHERE s.email = ?1")
  Optional<User> findByEmail(String email);

  @Transactional
  @Modifying
  @Query("UPDATE User u SET u.enabled = TRUE WHERE u.email = ?1")
  int enableUser(String email);

}
