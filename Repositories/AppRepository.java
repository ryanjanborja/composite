@Repository
@Transactional(readOnly = true)
public interface AppRepository extends JpaRepository<App, Long> {
}
