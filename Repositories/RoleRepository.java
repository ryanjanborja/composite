@Repository
@Transactional(readOnly = true)
public interface RoleRepository extends JpaRepository<Role, Long> {

  @Override
  Optional<Role> findById(@Param("id") Long id);

  Optional<Role> findByName(@Param("name") ERole name);

}
