@Getter
@NoArgsConstructor
@Entity(name = "App")
@Table(
  name = "app",
  uniqueConstraints = @UniqueConstraint(name = "app_name_unique", columnNames = "name")
)
public class App implements Serializable {

  private static final long serialVersionUID = -4933570661658603015L;

  @Id
  @SequenceGenerator(name = "app_sequence", sequenceName = "app_sequence", allocationSize = 1)
  @GeneratedValue(strategy = SEQUENCE, generator = "app_sequence")
  @Column(name = "id", updatable = false)
  private Long id;

  @Column(name = "name", nullable = false, columnDefinition = "TEXT")
  private String name;

  @OneToMany(
    mappedBy = "app",
    orphanRemoval = true,
    cascade = {PERSIST, REMOVE},
    fetch = EAGER
  )
  private List<Role> roles;

}
